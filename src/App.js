import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, useHistory} from 'react-router-dom'
import Header from './Components/SubComponents/Header'
import Notify from './Components/Notify/Notify'
import UserList from './Components/UserList/UserList'
import ValuationList from './Components/ValutaionList/ValuationList'
import Verify from './Components/Verify/Verify'
import Login from './Components/Login/Login'
import Store from './Components/redux/configureStore'
import { Provider } from 'react-redux'


const store = Store()
function App(props) {
  console.log(this)
  return (
    <Provider store={store} >
      <Router>
        <Header />
        <Route exact path='/' component={Login} />
        <Route path='/notify' component={Notify} />
        <Route path='/user' component={UserList} />
        <Route path='/verify' component={Verify} />
        <Route path='/value' component={ValuationList} />
      </Router>
    </Provider>
    
  );
}

export default App;
