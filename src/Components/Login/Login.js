import React,{useEffect, useState} from 'react'
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {useHistory} from 'react-router-dom'
import {actionCreators as authActions} from '../redux/modules/auth'
import {useDispatch, useSelector} from 'react-redux'
const useStyles = makeStyles(theme => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: 200,
      },
    },
  }));
export default function Login(){
    const dispatch = useDispatch()

    const [id, setId] = useState(null)
    const [password, setPassword] = useState(null)
    
    const getLogin=()=>{
        dispatch(authActions.userAuth(id,password))
        dispatch(authActions.getProfile())
        if (info.authInfoReducer.profile==id){
          history.push('/verify')
        }

      }
    const history = useHistory()
    const info = useSelector(store=>store)
    const classes = useStyles
    return (
    <div className='container' style={{width:'100%',textAlign:'center'}}>
        <form className={classes.root} noValidate autoComplete="off" style={{ width:'100%', marginTop:'25%'}}>
        <TextField value={id||''} label="Id" variant="outlined" onChange={e=>{setId(e.target.value)}} />
        </form>

        <form className={classes.root} noValidate autoComplete="off" style={{width:'100%'}}>
        <TextField type='password' value={password||''} label="Password" variant="outlined" onChange={e=>setPassword(e.target.value)} />
        </form>

        <Button onClick={e=>getLogin()}>로그인</Button>
    </div>
    
    )
}