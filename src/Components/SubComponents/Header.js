import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
    root: {
      flexGrow: 1,
    },
  });
export default function Header(){
    const classes = useStyles();
    let history = useHistory();

    const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    if (newValue==0){
        history.push('/user')
    }else if (newValue==1){
        history.push('/value')
    }else if (newValue==2){
        history.push('/verify')
    }else if (newValue==3){
        history.push('/notify')
    }
  };

    return (
        <Paper className={classes.root}>
            <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            centered
            >
            <Tab label="사용자 목록"  />
            <Tab label="평가 목록" />
            <Tab label="승인 목록" />
            <Tab label="공지사항" />
            </Tabs>
        </Paper>
    )
}