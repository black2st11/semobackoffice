import React,{useEffect, useState} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios'
import Header from '../SubComponents/Header';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});
export default function UserList(){
    const classes = useStyles
    const [data, setData] = useState()
    useEffect(()=>{
        getUserList()
    },[])
    const getUserList = () => {
        axios({
            method:"GET",
            url:'http://localhost:8000/manager/user/',
            headers : {
                "Content-type" : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>{setData(res.data)})
    }

    return (
        <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>id</TableCell>
            <TableCell>username</TableCell>
            <TableCell>email</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {data?data.map((value, index)=>{
              return(
                <TableRow key={index}>
                    <TableCell component='th' scope='row'>{value.id}</TableCell>
                    <TableCell>{value.username}</TableCell>
                    <TableCell>{value.email}</TableCell>
                </TableRow>
              )
          }):null}
        </TableBody>

      </Table>
    </TableContainer>
    )
}