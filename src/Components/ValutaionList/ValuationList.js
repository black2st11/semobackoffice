import React,{useEffect, useState} from 'react'
import axios from 'axios'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
export default function ValuationList(){
    const classes = useStyles
    const [data, setData] = useState();
    useEffect(()=>{
        getUserList()
    },[])
    const getUserList = () => {
        axios({
            method:"GET",
            url:'http://localhost:8000/manager/value/',
            headers : {
                "Content-type" : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>setData(res.data))
    }
    console.log(data)
    return (
        <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>id</TableCell>
            <TableCell>name</TableCell>
            <TableCell>price</TableCell>
            <TableCell>image</TableCell>
            <TableCell>owner</TableCell>
            <TableCell>승인여부</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {data?data.map((value, index)=>{
              return(
                <TableRow key={index}>
                    <TableCell component='th' scope='row'>{value.id}</TableCell>
                    <TableCell>{value.name}</TableCell>
                    <TableCell>{value.price}</TableCell>
                    <TableCell><img src={'http://localhost:8000/media/'+value.image} style={{width:100,height:100}}/></TableCell>
                    <TableCell>{value.owner}</TableCell>
                    <TableCell>{value.is_verify.toString()}</TableCell>
                </TableRow>
              )
          }):null}
        </TableBody>

      </Table>
    </TableContainer>
    )
}