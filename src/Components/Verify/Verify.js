import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios'
import Header from '../SubComponents/Header';
import Button from '@material-ui/core/Button';
import {actionCreators as verifyAction} from '../redux/modules/verify' 

import { useDispatch, useSelector } from 'react-redux'
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function Verify(){
    const classes = useStyles
    const [data, setData] = useState();
    const dispatch = useDispatch();
    useEffect(()=>{
        getVerifyList()
      setData(info.verifyReducer.verifyList)

    },[])
    const getVerifyList = () => {
        dispatch(verifyAction.getVerify())
        // axios({
        //     method:"GET",
        //     url:'http://localhost:8000/manager/verify/',
        //     headers : {
        //         "Content-type" : 'application/json',
        //         'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        //     }
        // }).then(res=>{setData(res.data)})
    }
    const info = useSelector(store=>store)
    console.log(info)
    const putVerify= (id) =>{
        axios({
            method:"PUT",
            url : `http://localhost:8000/manager/verify/${id}/`,
            headers : {
                "Content-type" : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>console.log(res))
    }
    
    const delVerify= (id) =>{
        axios({
            method:"DELETE",
            url : `http://localhost:8000/manager/verify/${id}/`,
            headers : {
                "Content-type" : 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            }
        }).then(res=>console.log(res))
    } 

    return (
        <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>id</TableCell>
            <TableCell>name</TableCell>
            <TableCell>price</TableCell>
            <TableCell>image</TableCell>
            <TableCell>owner</TableCell>
            <TableCell>승인여부</TableCell>
            <TableCell>삭제</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {data?data.map((value, index)=>{
              return(
                <TableRow key={index}>
                    <TableCell component='th' scope='row'>{value.id}</TableCell>
                    <TableCell>{value.name}</TableCell>
                    <TableCell>{value.price}</TableCell>
                    <TableCell><img src={'http://localhost:8000/media/'+value.image} style={{width:100,height:100}}/></TableCell>
                    <TableCell>{value.owner}</TableCell>
                    <TableCell>
                        <Button variant="contained" color="primary" onClick={e=>putVerify(value.id)}>
                            승인
                        </Button>
                    </TableCell>
                    <TableCell>
                        <Button variant="contained" color="secondary" onClick={e=>delVerify(value.id)}>
                            삭제
                        </Button>
                    </TableCell>
                </TableRow>
              )
          }):null}
        </TableBody>

      </Table>
    </TableContainer>
    )
}