import { createEpicMiddleware, combineEpics } from 'redux-observable';

import { combineReducers, createStore, applyMiddleware} from 'redux';

import { authInfoReducer, userAuthEpic, getProfileEpic  } from './modules/auth'
import { getVerifyEpics, verifyReducer } from './modules/verify'
const rootReducers = () => combineReducers({
    authInfoReducer,
    verifyReducer
})

const rootEpics =  combineEpics(
    userAuthEpic,
    getProfileEpic,
    getVerifyEpics
)

const epicMiddleware = createEpicMiddleware();

export default function configureStore() {
    const store = createStore(
        rootReducers(),
        applyMiddleware(epicMiddleware)
    );

    epicMiddleware.run(rootEpics);

    return store;
};