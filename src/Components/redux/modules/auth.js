import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { mergeMap, map, catchError, withLatestFrom } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { notification } from 'antd';
import { useHistory } from 'react-router-dom'
// 개발환경 
const env = process.env.NODE_ENV

const USER_AUTH = 'USER_AUTH'
const USER_AUTH_SUCCESS = "USER_AUTH_SUCCESS"
const USER_AUTH_FAIL = "USER_AUTH_FAIL"
const LOGOUT = "LOGOUT"


// 유저 로그인 액션
const userAuth = (username, password) =>({
    type : USER_AUTH,
    payload : {
        username : username,
        password : password
    }
});

// 로그인 성공 액션
const userAuthSuccess = (access_token, refresh_token) => {
    // 로그인 성공시 공지 페이지로 이동
    return ({
        // Token 저장
        type: USER_AUTH_SUCCESS,
        payload : {
            access_token : access_token,
            refresh_token : refresh_token
        }
    })
};

// 로그인 실패
const userAuthFail = (payload) => ({
    type : USER_AUTH_FAIL,
    payload
})

// 로그아웃
const logout = () => {
    return ({
        type : LOGOUT
    })
}

// 유저 정보관련 액션
const GET_PROFILE = 'GET_PROFILE'
const GET_PROFILE_SUCCESS = 'GET_PROFILE_SUCCESS'
const GET_PROFILE_FAIL = 'GET_PROFILE_FAIL'

const getProfile = () => ({
    type : GET_PROFILE
})

const getProfileSuccess = (payload) =>({
    type : GET_PROFILE_SUCCESS,
    payload : payload
})

const getProfileFail = (payload) => ({
    type : GET_PROFILE_FAIL,
    payload : payload
})
const actionCreators = {
    userAuth,
    logout,
    getProfile
}
export {actionCreators}

let endpoint = {
    client_id : 'WsckrtEQtdj9IzWm2sCZhwfdROv9ty7Dnk4qJIeP',
    secret_id : '7uulbYH6LbTsbQhiAC4GrzTEIvDZs1VmDAM5qCJDFjV1zrZdqt6unUuUWwxjaQ3mH8oPNtbvSrwCEAhSv3aBZKRnyA0G4yvmUaPidSKueAWQ2d96M6oxg4GjRDO180vD',
    url : 'http://localhost:8000'
}
// 개발환경일 때 endpoint
if (env == 'development'){
    endpoint = {
        client_id : 'WsckrtEQtdj9IzWm2sCZhwfdROv9ty7Dnk4qJIeP',
        secret_id : '7uulbYH6LbTsbQhiAC4GrzTEIvDZs1VmDAM5qCJDFjV1zrZdqt6unUuUWwxjaQ3mH8oPNtbvSrwCEAhSv3aBZKRnyA0G4yvmUaPidSKueAWQ2d96M6oxg4GjRDO180vD',
        url : 'http://localhost:8000'
    }
}

// 유저관련 초기상태값
const authInfoInitialState ={
    endpoint : endpoint,
    authInfo : {
        access_token : localStorage.getItem('access_token'),
        refresh_token : localStorage.getItem('refresh_token'),
        apiUrl : endpoint.url
    },
    profile : localStorage.getItem('username'),
    isLoggedIn : localStorage.getItem('isLoggedIn')
}


// 유저관련 리듀서
export const authInfoReducer = (state = authInfoInitialState, action) =>{
    switch(action.type){
        case USER_AUTH:
            return state;
        case USER_AUTH_SUCCESS:
            localStorage.setItem('access_token', action.payload.access_token);
            localStorage.setItem('refresh_token', action.payload.refresh_token);
            localStorage.setItem('isLoggedIn', true)
            return {
                ...state,
                authInfo :{
                    ...state.authInfo,
                    access_token : action.payload.access_token,
                    refresh_token : action.payload.refresh_token,
                    apiUrl : endpoint.url
                },
                isLoggedIn : true
            };
        case USER_AUTH_FAIL:
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('isLoggedIn')
            localStorage.removeItem('username')
            return {
                ...state,
                authInfo :{
                    ...state.authInfo,
                    access_token : null,
                    refresh_token : null
                },
                username : null,
                isLoggedIn : null
            };
        case LOGOUT:
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            localStorage.removeItem('isLoggedIn')
            localStorage.removeItem('username')
            return{
                ...state,
                authInfo : {
                    ...state.authInfo,
                    access_token : null,
                    refresh_token : null,
                    apiUrl : null
                },
                username : null,
                isLoggedIn : null
        }
        case GET_PROFILE:
            return{
                ...state
            }
        case GET_PROFILE_SUCCESS:
            console.log(action)
            localStorage.setItem('username',action.payload)
            return {
                ...state,
                username : localStorage.getItem('username')
            }
        case GET_PROFILE_FAIL:
            localStorage.removeItem('username')
            return {
                ...state,
                username : null
            }
        default:
            return state;
    }
}

// 에픽
export const userAuthEpic = (action$) => action$.pipe(
    ofType(USER_AUTH),
    mergeMap(action => {
        
        let id = action.payload.username
        let password = action.payload.password
        let client_id = endpoint.client_id
        let secret_id = endpoint.secret_id
        let _data = "grant_type=password&username="+id+"&password="+password+"&client_id="+client_id+"&secret_id="+secret_id
        return ajax({
            url : `${endpoint.url}/manager/token/`,
            method : 'POST',
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body : _data,
            withCredentials : true,
            timeout :10000
        }).pipe(
            map(res => {
                console.log(res)
                return (userAuthSuccess(res.response.access_token, res.response.refresh_token))
            }),
            catchError(err => {
                console.log(err)
                return of({
                    type : USER_AUTH_FAIL,
                    payloald : err.status
                })
            })
        )
    })
)

export const getProfileEpic = (action$, state$) => action$.pipe(
    ofType(GET_PROFILE),
    withLatestFrom(state$),
    mergeMap(([action,state])=>{
        return ajax({
            method : 'GET',
            url : `${endpoint.url}/user/`,
            headers : {
                'Content-type' : 'application/json',
                'Authorization' : `Bearer ${state.authInfoReducer.authInfo.access_token}`
            }
        }).pipe(
            map(res=>{
                console.log(res)
                return getProfileSuccess(res.response.username)
            }),
            catchError(err=>{
                return getProfileFail()
            })
        )
    })
)

