import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { mergeMap, map, catchError, withLatestFrom } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { notification } from 'antd';
import { useHistory } from 'react-router-dom'

const GET_VERIFY = 'GET_VERIFY'
const GET_VERIFY_SUCCESS = 'GET_VERIFY_SUCCESS'
const GET_VERIFY_FAIL = 'GET_VERIFY_FAIL'


const getVerify = (payload) =>({
    type:GET_VERIFY,
    payload
})

const getVerifySuccess = (payload) =>({
    type:GET_VERIFY_SUCCESS,
    payload
})

const getVerifyFail = (payload) =>({
    type:GET_VERIFY_FAIL,
    payload
})

const actionCreators = {
    getVerify
}

export {actionCreators}

export const initialState = {
    list : null
}

export const verifyReducer = (state= initialState, action) =>{
    switch(action.type){
        case GET_VERIFY:
            return {...state}
        case GET_VERIFY_SUCCESS:
            return {
                verifyList : action.payload
            }
        case GET_VERIFY_FAIL:
            return {
                verifyList : null
            }
        default:
            return{
                verifyList : null
            }
    }
} 

export const getVerifyEpics = (action$, state$) => action$.pipe(
    ofType(GET_VERIFY),
    withLatestFrom(state$),
    mergeMap(([action, state]) =>{
        return ajax({
            url : `${state.authInfoReducer.authInfo.apiUrl}/manager/verify/`,
            method : 'GET',
            headers : {
                'Content-type' :'application/json',
                'Authorization' : `Bearer ${state.authInfoReducer.authInfo.access_token}`
            }
        }).pipe(
            map(res=>{
                console.log(res)
                return getVerifySuccess(res.response)
            }),
            catchError(err=>{
                console.log(err)
                return getVerifyFail()
            })
        )
    })
)

